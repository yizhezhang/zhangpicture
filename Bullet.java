
public class Bullet {
	private static final int initial = 10;
	private int remain;
	
	//Constructor for Bullet 
	//Set the magazine size to 10 and set remain of bullet to 10
	public Bullet () {
		remain = initial;
	}
	
	//Accessor method of the number of bullet remain in the magazine 
	public int getRemain() {
		return remain;
	}
	
	//Each time fire a bullet, the remain bullet will decrease by 1
	public void fire() {
		remain--;
	}
	
	//Check if there is any bullet in the magazine 
	public boolean hasRemain() {
		return remain>0;
	}
}
