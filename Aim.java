import java.util.Random;

public class Aim {
	private Random randomGenerator;
	private static final int numberPlayed = 5;
	private static final int radius = 50;
	private int x;
	private int y;
	private int clicked;
	
	//Constructor for aim
	public Aim() {
		randomGenerator = new Random();
		x = 200;
		y = 200;
		clicked = 0;
	}
	
	//Accessor method of x position of aim 
	public int getX() {
		return x;
	}
	
	//Accessor method of y position of aim 
	public int getY() {
		return y;
	}
	
	//Accessor method of radius of aim 
	public int getRadius() {
		return radius;
	}
	
	//Accessor method of number of aim get shot
	public int getClicked() {
		return clicked;
	}
	
	//One aim get shoot and change the place of aim
	public void shoot() {
		clicked++;
		x = randomGenerator.nextInt(1570)+50;
		y = randomGenerator.nextInt(1570)+50;
	}
	
	public boolean finished() {
		if(clicked < numberPlayed) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean withInRange(int xPosition, int yPosition) {
		return xPosition>=(x-radius) && xPosition <= (x+radius) && yPosition>=(y-radius) && 
				yPosition <=(y+radius);
	}
	
}
