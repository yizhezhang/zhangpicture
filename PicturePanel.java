/**
 * PicturePanel.java
 * Author: Chuck Cusack
 * Date: August 22, 2007
 * Version: 2.0
 * 
 * Modified August 22, 2008
 *
 *An almost blank picture.
 *It just draws a few things.
 *
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.awt.Image;

import javax.swing.JPanel;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * A class draws a picture on a panel
 *
 */
public class PicturePanel extends JPanel 
{
    // A sample field.  I just keep track of number of mouse clicks.
    int numberOfClicks;
    private Aim target;
    private int xMouse = 0;
    private int yMouse = 0;
    private long startTime = 0;
    private long finishTime=0;
    private Image gun, background;
    private static int i = 1; //Parameter used for allowing one action only happen once 
    private long time;
    private Bullet bullet;
    private HashMap<String, Aim> allAim;
    private Aim falseTarget;
    
    /**
     * Get stuff ready so when paintComponent is called, it can draw stuff.
     * Depending on how complicated you want to get, your constructor may be blank.
     */
    public PicturePanel() 
    {
        // If you want to handle mouse events, you will need the following
        // 3 lines of code.  Just leave them as is and modify the methods
        // with "mouse" in the name toward the end of the class.
        // If you don't want to deal with mouse events, delete these lines.
        MouseHandler mh=new MouseHandler();
        addMouseListener(mh);
        addMouseMotionListener(mh);
        
        // Initialize number of mouse clicks to 0.
        numberOfClicks=0;
        target = new Aim();
        falseTarget = new Aim();
        bullet = new Bullet();
        allAim = new HashMap<String, Aim>();
        allAim.put("1", target);
        allAim.put("2", falseTarget);
        
        //Import image into the program
        try {
			gun = ImageIO.read(this.getClass().getResource("gun.png"));
			background = ImageIO.read(this.getClass().getResource("background 2.png"));
		} catch (IOException e) {

		}
    }

    /**
     * This method is called whenever the applet needs to be drawn.
     * This is the most important method of this class, since without
     * it, we don't see anything.
     * 
     * This is the method where you will most likely do all of your coding.
     */
    public void paintComponent(Graphics g) 
    {
        // Always place this as the first line in paintComponent.
        super.paintComponent(g);
        
        // Draw background element 
        g.drawImage(background, 0, 0, null);
        g.drawImage(gun, 1620, 1620, null);
        g.setColor(Color.green);
        g.setFont(new Font("LucidaSans",Font.BOLD,40));
    	g.drawString("Yizhe Zhang", 1600, 1900);
 
	    // Draw a string which tells how many mouse clicks
	    g.setColor(Color.green);
	    g.setFont(new Font("Times Roman",Font.BOLD,24));
        g.drawString("Shoot at green unless there is not gree",40,40);
        
        //Draw aim 
        g.setColor(Color.green);
        g.fillOval(allAim.get("1").getX(), allAim.get("1").getY(), allAim.get("1").getRadius(), allAim.get("1").getRadius());

        //Draw falseAim 
        g.setColor(Color.red);
        g.fillOval(allAim.get("2").getX(), allAim.get("2").getY(), allAim.get("2").getRadius(), allAim.get("2").getRadius());
        
        //Draw cursor 
        g.setColor(Color.red);
        g.fillOval(xMouse, yMouse, 10, 10);
        
        //Draw bullet related element 
        //Draw indication String 
        g.setColor(Color.green);
        g.setFont(new Font("LucidaSans",Font.BOLD,50));
    	g.drawString("Here is your Bullte", 50, 1600);
        
    	//Draw acutal bullet 
        int a = 0;
        int r = bullet.getRemain();
        g.setColor(Color.yellow);
        while (a<r) {
        	if(r>5) {
        		int p = 0;
            	while (p<5) {
            		g.fillRect(50+30*p, 1700, 20, 50);
            		p++;
            	}
            	r=r-5;
        	}
        	g.fillRect(50+30*a, 1800, 20, 50);
        	a++;
        }
        
        //Draw indication of empty magazine 
        if (!bullet.hasRemain()) {
        	g.setColor(Color.green);
        	g.setFont(new Font("Arial",Font.BOLD,50));
        	g.drawString("You do not have any bullet left", 50, 1800);
        }
        
        //Draw finish element
        if (!target.finished()) {
        	g.setColor(Color.white);
        	g.setFont(new Font("LucidaSans",Font.BOLD,50));
        	g.drawString("You finished the game", 1000, 1000);
        	finishTime = System.currentTimeMillis();
        	if (i==1) {
        		time = finishTime - startTime;
        		i++;
        	}
        	g.drawString("Your total time is "+Long.toString(time)+"ms", 1000, 1100);
        }
    }
    //------------------------------------------------------------------------------------------
    
     //---------------------------------------------------------------
    // A class to handle the mouse events for the applet.
    // This is one of several ways of handling mouse events.
    // If you do not want/need to handle mouse events, delete the following code.
    //
    private class MouseHandler extends MouseAdapter implements MouseMotionListener
    {
      
        public void mouseClicked(MouseEvent e) 
        {
            // Increment the number of mouse clicks
            numberOfClicks++;
            
            //After mouse get clicked on bullet will get fired 
            bullet.fire();
            
            //Start counting time 
            if(numberOfClicks == 1) {
            	startTime = System.currentTimeMillis();
            }
            
            //Check if shoot at correct place and change the location of aim 
            int xPosition = e.getX();
            int yPosition = e.getY();
            if (allAim.get("1").finished()&&bullet.hasRemain()) {
            	if(allAim.get("1").withInRange(xPosition, yPosition)) {
            		allAim.get("1").shoot();
            	}
            	repaint();
            }
            if(allAim.get("2").withInRange(xPosition, yPosition)) {
            	allAim.get("2").shoot();
            	repaint();
            }
        }

        public void mouseEntered(MouseEvent e)
        {
        }
        public void mouseExited(MouseEvent e) 
        {
        }        
        public void mousePressed(MouseEvent e) 
        {
            
        }        
        public void mouseReleased(MouseEvent e) 
        {
        }
        public void mouseMoved(MouseEvent e) 
        {
        	//A red dot will appear at the position of mouse to indicate where will shoot 
        	if (target.finished()) {
        	xMouse = e.getX();
        	yMouse = e.getY();
        	repaint();
        	}
        }
        public void mouseDragged(MouseEvent e) 
        {
        }
    }
    // End of MouseHandler class
    
} // Keep this line--it is the end of the PicturePanel class